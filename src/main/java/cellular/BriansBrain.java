
package cellular;

import javax.print.attribute.standard.RequestingUserName;

import datastructure.CellGrid;


public class BriansBrain extends GameOfLife {
    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */


    public BriansBrain(int rows, int columns) {
        super(rows, columns); 
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }


    @Override
    public CellState getNextCell(int row, int col) {
        
        int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState status = getCellState(row, col);

		if ( status.equals(CellState.ALIVE)) 
			return CellState.DYING; 
		else if (status.equals(CellState.DEAD) && livingNeighbors == 2) 
			return CellState.ALIVE;
        else return CellState.DEAD;

        
        
        }
    
    }



