package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    private int columns;
    private int rows;
    CellState[][] grid = new CellState[0][0];
    private CellState initialState ;

    

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows] [columns];

        for (int row = 0; row < rows; row++){
            for (int column = 0; column < columns; column++){
                grid[row][column] = initialState;
                
                

            }
        }



	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // // TODO Auto-generated method stub
        // if (row >= 0 && row < numRows()) {
        //     if (column >= 0 && column < numColumns()) {
        //         grid[row][column] = element; 
        //     }}
        grid[row][column]= element;

    }

        
    

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(rows, columns, initialState);
        for (int row = 0; row < rows; row++){
            for (int column = 0; column < columns; column++){
                newGrid.set(row, column, grid[row][column]);
                
                

            }
        }
        return newGrid;
        
    }
    
}
